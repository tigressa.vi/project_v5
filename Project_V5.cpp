
#include <iostream>
#include <Windows.h> 

int day()
{
    SYSTEMTIME st;
    GetLocalTime(&st);

    return st.wDay;
}

int main()
{
    int sum = 0;
    const int n = 4;
    const int q = n;
    const int w = n;
    int z = day() % n;

    int a[q][w];

    for (int i = 0; i < q; i++)
    {
        for (int j = 0; j < w; j++)
        {
            a[i][j] = i + j;
        }
    }
    for (int i = 0; i < q; i++)
    {
        for (int j = 0; j < w; j++)
        {
            std::cout << a[i][j] << " ";
        }
        std::cout << std::endl;
    }
    for (int i = 0; i < q; i++)
    {
        for (int j = 0; j < w; j++)
        {
            if (i == z) sum += j;
        }
    }
    std::cout << "Sum = " << sum;
}